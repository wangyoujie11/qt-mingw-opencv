#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    QString filename=QFileDialog::getOpenFileName(this,"打开图像文件","Image File(*.bmp;*.png;*.jpg)");
       if(filename == "")
       {
           QMessageBox::information(this,"提示","文件打开失败!");
           return;
       }
       img_input = cv::imread(cv::String(filename.toLocal8Bit().toStdString()));
       if(img_input.empty())
       {
           QMessageBox::information(this,"提示","文件打开失败!");
           return;
       }
       cvtColor(img_input, gray_img, COLOR_BGR2GRAY);

       Mat temp;
       QImage qImg;

       if (!isGray)
       {
           cv::cvtColor(img_input,temp,COLOR_BGR2RGB);
           qImg = QImage((const unsigned char*)(temp.data), temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
       }
       else
       {
           cvtColor(gray_img, temp, COLOR_GRAY2RGB);//GRAY convert to RGB
           qImg = QImage((const unsigned char*)(temp.data), temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
       }
       ui->label->setPixmap(QPixmap::fromImage(qImg));
       ui->label->setScaledContents(true);

}

void Widget::on_checkBox_clicked()
{
    if (ui->checkBox->isChecked())
        {
            isGray = 1;
        }
        else
        {
            isGray = 0;
        }
}
