#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <opencv2/opencv.hpp>
using namespace cv;

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    Mat img_input; //原始图片
    Mat gray_img; //灰度图片
    int isGray = 0; //判断是否设置为灰度图

private slots:
    void on_pushButton_clicked();

    void on_checkBox_clicked();

private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
